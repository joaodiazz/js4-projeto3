let usuario = {
  nome: "João",
  sobrenome: "Dias",
  idade: 21,
};

let frutas = ["uva", "banana", "laranja"];

function objProps(obj) {
  for (i in obj) {
    console.log(i);
  }
}

function liArr(arr) {
  for (i in arr) {
    console.log(arr[i]);
  }
}

objProps(usuario);
liArr(frutas);
